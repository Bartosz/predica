Stwórz aplikację Web korzystająca z Web API i bazą danych SQL, pozwalająca na wykonanie poniższych funkcjonalności:

Standardowy użytkownik: może przeglądać dane pogodowe zapisane w bazie danych
Administrator: może odświeżać dane pogodowe z bazy danych za pomocą zewnętrznych serwisów
Technologie do wykorzystania: .NET (może być MVC), JavaScript, HTML, Web Services

"Standardowy użytkownik" przegląda dane meterologiczne, czyli 4 dowolne parametry dla jednego z 15 miast w Polsce w jednym z 2 trybów:

- dane najświeższe
- zestawienie historyczne (średnia pawartość parametrów dla wybranego dnia) // brak

"Administrator" odswieża wszystkie dane meterologiczne dla wszystkich zdefiniowanych lokalizacji wskazując zakres daty.  
Dodatkowo zostaje poinformowany na stronie, iż dane zostały odświeżone. // brak
Widoki użytkownika powinny otwierać się w 3 sekundy (od kliknięcia do załadowania strony)



Adres: https://predicaforecasterweb.azurewebsites.net/

Konto administratora:
Login: administrator@predica.com
Hasło: Predica2018@

Accuweather posiada limit 50 requestów dziennie. 
