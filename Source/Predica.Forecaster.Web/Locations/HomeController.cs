﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Predica.Forecaster.Web.Infrastructure;
using Predica.Forecaster.Web.Infrastructure.Data;

namespace Predica.Forecaster.Web.Locations
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;
        private readonly IMemoryCache _cache;

        public HomeController(ApplicationDbContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<LocationViewModel> locations;

            if (!_cache.TryGetValue("Locations", out List<LocationViewModel> viewModel))
            {
                locations = _context.Location.Select(x => new LocationViewModel()
                {
                    LocationId = x.LocationId,
                    Name = x.Name
                }).ToList();

                _cache.Set("Locations", locations);
            }
            else
            {
                locations = viewModel;
            }


            ViewBag.Locations = locations;

            return View();
        }

        [HttpPost]
        public IActionResult Index(LocationViewModel location)
        {
            ViewBag.SelectedValue = location.LocationId;
            return RedirectToAction("CurrentMeasurements", "Measurements",  new{locationId = location.LocationId});
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
