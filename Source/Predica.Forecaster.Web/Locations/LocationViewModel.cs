﻿namespace Predica.Forecaster.Web.Locations
{
    public class LocationViewModel
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
    }
}