﻿using Microsoft.AspNetCore.Mvc;
using Predica.Forecaster.Web.Infrastructure;
using Predica.Forecaster.Web.Infrastructure.Data;

namespace Predica.Forecaster.Web.Measurements
{
    [Route("measurements")]
    public class MeasurementsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly MeasurementsReporter _service;

        public MeasurementsController(ApplicationDbContext context)
        {
            IDateTimeProvider dateProvider = new UtcDateTimeProvider();
            _context = context;
            _service = new MeasurementsReporter(dateProvider, _context);
        }

        [HttpGet]
        [Route("current/{locationId}")]
        public IActionResult CurrentMeasurements([FromRoute]int locationId)
        {
            HourlyMeasurement result = _service.QueryCurrentMeasurement(locationId);

            if (result == null)
            {
                
            }
            var viewModel = new CurrentMeasurementViewModel
            {
                LocationId = result.LocationId, 
                FeelsTemperature = result.FeelsTemperature,
                Humidity = result.Humidity,
                LocationName = result.Location.Name,
                Temperature = result.Temperature,
                WindSpeed = result.WindSpeed
            };

            return View(viewModel);
        }

        //[HttpGet()]
        //[Route("history/locationId")]
        //public IActionResult HistoricalMeasurements([FromRoute]int locationId)
        //{
        //    var queryHistoricalMeasurements = _service.QueryHistoricalMeasurements(locationId);
        //    return Ok();
        //}
    }
}