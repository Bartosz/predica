﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Predica.Forecaster.Web.Infrastructure;
using Predica.Forecaster.Web.Infrastructure.Data;

namespace Predica.Forecaster.Web.Measurements
{
    public class MeasurementsReporter
    {
        private readonly IDateTimeProvider _timeProvider;
        private readonly ApplicationDbContext _context;

        public MeasurementsReporter(IDateTimeProvider timeProvider, ApplicationDbContext context)
        {
            _timeProvider = timeProvider;
            _context = context;
        }

        public HourlyMeasurement QueryCurrentMeasurement(int locationId)
        {
            var last3hours = ((DateTimeOffset)_timeProvider.Now().AddHours(-3)).ToUnixTimeSeconds();

            var currentConditions = _context.HourlyMeasurement
                .Where(x => x.LocationId == locationId && x.MeasuredDate >= last3hours)
                .Include(x => x.Location)
                .OrderByDescending(x => x.MeasuredDate)
                .FirstOrDefault();

            return currentConditions;
        }

        public HourlyMeasurement QueryHistoricalMeasurements(int locationId)
        {
            //const int ONE_WEEK = 7;

            //var now = _timeProvider.Now();
            //var nowTimestamp = _timeProvider.ToTimestamp(now);
            //var weekAgo = _timeProvider.ToTimestamp(now.Date.AddDays(-ONE_WEEK));

            //var currentConditions = _context.HourlyMeasurement
            //    .Where(x => x.LocationId == locationId && weekAgo >= x.MeasuredDate && x.MeasuredDate >= nowTimestamp)
            //    .OrderBy(x => x.MeasuredDate).ToList()

            ////currentConditions.Select(x => x.MeasuredDate)



            //return currentConditions;

            return new HourlyMeasurement();
        }
    }
}