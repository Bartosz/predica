﻿using System.ComponentModel.DataAnnotations;

namespace Predica.Forecaster.Web.Measurements
{
    public class CurrentMeasurementViewModel
    {
        [Display(Name = "Location name")]
        public string LocationName { get; set; }
        [Display(Name = "Temperature °C")]
        public double Temperature { get; set; }
        [Display(Name = "Feels like temperature °C")]
        public double FeelsTemperature { get; set; }
        [Display(Name = "Humidity %")]
        public decimal Humidity { get; set; }
        [Display(Name = "WindSpeed km/h")]
        public double WindSpeed { get; set; }

        public int LocationId { get; set; }
    }
}