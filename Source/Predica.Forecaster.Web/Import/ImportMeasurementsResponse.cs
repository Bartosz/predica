﻿namespace Predica.Forecaster.Web.Import
{
    public class ImportMeasurementsResponse
    {
        public int LocationId { get; set; }
        public long MeasuredDate { get; set; }
        public double Temperature { get; set; }
        public double FeelTemperature { get; set; }
        public double WindSpeed { get; set; }
        public decimal Humidity { get; set; }
    }
}