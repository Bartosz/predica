﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Predica.Forecaster.Web.Import.Accuweather;
using Predica.Forecaster.Web.Import.Commands;
using Predica.Forecaster.Web.Import.ViewModels;
using Predica.Forecaster.Web.Infrastructure;
using Predica.Forecaster.Web.Infrastructure.Data;

namespace Predica.Forecaster.Web.Import.Controllers
{
    [Route("import")]
    [Authorize(Roles ="Administrator")]
    public class ImporterController : Controller
    {
        private readonly ImportMeasurementsCommandHandler _handler;

        public ImporterController(ApplicationDbContext context)
        {
            IDateTimeProvider dateProvider = new UtcDateTimeProvider();
            IMeasurementsRepository repository = new AccuweatherImporter(context);
            _handler = new ImportMeasurementsCommandHandler(context, repository, dateProvider);
        }

        [HttpPost("measurements")]
        public IActionResult ImportMeasurements(ImportMeasurementsViewModel viewModel)
        {
            try
            {
                _handler.ImportMeasurements(new ImportMeasurementsCommand()
                {
                    FromDateTime = viewModel.FromDatetime,
                    TillDateTime = viewModel.TillDateTime
                });
            }
            catch (Exception e)
            {
                return View(new ErrorViewModel
                { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet()]
        public IActionResult Index()
        {
            return View();
        }
    }
}