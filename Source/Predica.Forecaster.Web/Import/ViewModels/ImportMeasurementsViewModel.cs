﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Predica.Forecaster.Web.Import.ViewModels
{
    public class ImportMeasurementsViewModel
    {
        [Display(Name ="From date")]
        public DateTime FromDatetime { get; set; }
        [Display(Name = "To date")]
        public DateTime TillDateTime { get; set; }        
    }
}