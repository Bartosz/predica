﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Predica.Forecaster.Web.Infrastructure;
using Predica.Forecaster.Web.Infrastructure.Data;

namespace Predica.Forecaster.Web.Import.Commands
{
    public class ImportMeasurementsCommandHandler
    {
        private readonly ApplicationDbContext _context;
        private readonly IMeasurementsRepository _repository;
        private readonly IDateTimeProvider _timeProvider;

        public ImportMeasurementsCommandHandler(ApplicationDbContext context, IMeasurementsRepository repository,
            IDateTimeProvider timeProvider)
        {
            _context = context;
            _repository = repository;
            _timeProvider = timeProvider;
        }

        public void ImportMeasurements(ImportMeasurementsCommand interval)
        {
            if (interval.FromDateTime > interval.TillDateTime)
                throw new InvalidOperationException();

            List<int> locations = GetLocations();
            var forecastPerHours = GetForecast(interval, locations);
            var historyPerHours = GetHistory(interval, locations);


            if (forecastPerHours != null)
            {
                foreach (var perHour in forecastPerHours)
                {
                    var exists = _context.HourlyMeasurement.AsNoTracking().Any(x => x.LocationId == perHour.LocationId && x.MeasuredDate == perHour.MeasuredDate);
                    if (exists)
                    {
                        _context.HourlyMeasurement.Update(perHour);
                        continue;
                    }
                    _context.HourlyMeasurement.Add(perHour);
                }
            }

            if (historyPerHours != null)
            {

                foreach (var perHour in historyPerHours)
                {
                    var exists = _context.HourlyMeasurement.AsNoTracking().Any(x => x.LocationId == perHour.LocationId && x.MeasuredDate == perHour.MeasuredDate);
                    if (exists)
                    {
                        _context.HourlyMeasurement.Update(perHour);
                        continue;
                    }
                    _context.HourlyMeasurement.Add(perHour);
                }
            }

            _context.SaveChanges();
        }

        private IEnumerable<HourlyMeasurement> GetHistory(ImportMeasurementsCommand interval, List<int> locations)
        {
            var requestedHistory = _timeProvider.Now() > interval.FromDateTime;

            if (!requestedHistory)
                return new List<HourlyMeasurement>();
  
            var historicalMeasurements = _repository.GetHistoricalConditions(locations).ToList();
            var historyPerHours = historicalMeasurements.Select(f => new HourlyMeasurement
            {
                MeasuredDate = f.MeasuredDate,
                LocationId = f.LocationId,
                Temperature = f.Temperature,
                FeelsTemperature = f.FeelTemperature,
                Humidity = f.Humidity,
                WindSpeed = f.WindSpeed
            });
            return historyPerHours;
        }

        private IEnumerable<HourlyMeasurement> GetForecast(ImportMeasurementsCommand interval, List<int> locations)
        {
            var now = _timeProvider.Now();
            var requestedForecast = now <= interval.TillDateTime;

            if (!requestedForecast)
                return new List<HourlyMeasurement>();

            var futureMeasurements = _repository.GetForecastConditions(locations).ToList();
            var forecastPerHours = futureMeasurements.Select(f => new HourlyMeasurement
            {
                MeasuredDate = f.MeasuredDate,
                LocationId = f.LocationId,
                Temperature = f.Temperature,
                FeelsTemperature = f.FeelTemperature,
                Humidity = f.Humidity,
                WindSpeed = f.WindSpeed
            });
            return forecastPerHours;
        }

        private List<int> GetLocations() => _context.Location.Select(x => x.LocationId).ToList();
    }
}