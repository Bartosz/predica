﻿using System;

namespace Predica.Forecaster.Web.Import.Commands
{
    public class ImportMeasurementsCommand
    {
        public DateTime FromDateTime { get; set; }
        public DateTime TillDateTime { get; set; }
    }
}