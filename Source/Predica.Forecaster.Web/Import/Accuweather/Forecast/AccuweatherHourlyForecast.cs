﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Predica.Forecaster.Web.Import.Accuweather.Forecast
{
    public partial class AccuweatherHourlyForecast
    {
        [JsonProperty("DateTime")] public DateTimeOffset DateTime { get; set; }

        [JsonProperty("EpochDateTime")] public long EpochDateTime { get; set; }

        [JsonProperty("WeatherIcon")] public long WeatherIcon { get; set; }

        //[JsonProperty("IconPhrase")] public IconPhrase IconPhrase { get; set; }

        [JsonProperty("IsDaylight")] public bool IsDaylight { get; set; }

        [JsonProperty("Temperature")] public Ceiling Temperature { get; set; }

        [JsonProperty("RealFeelTemperature")] public Ceiling RealFeelTemperature { get; set; }

        [JsonProperty("WetBulbTemperature")] public Ceiling WetBulbTemperature { get; set; }

        [JsonProperty("DewPoint")] public Ceiling DewPoint { get; set; }

        [JsonProperty("Wind")] public Wind Wind { get; set; }

        [JsonProperty("WindGust")] public WindGust WindGust { get; set; }

        [JsonProperty("RelativeHumidity")] public long RelativeHumidity { get; set; }

        [JsonProperty("Visibility")] public Ceiling Visibility { get; set; }

        [JsonProperty("Ceiling")] public Ceiling Ceiling { get; set; }

        [JsonProperty("UVIndex")] public long UvIndex { get; set; }

        //[JsonProperty("UVIndexText")] public UvIndexText UvIndexText { get; set; }

        [JsonProperty("PrecipitationProbability")]
        public long PrecipitationProbability { get; set; }

        [JsonProperty("RainProbability")] public long RainProbability { get; set; }

        [JsonProperty("SnowProbability")] public long SnowProbability { get; set; }

        [JsonProperty("IceProbability")] public long IceProbability { get; set; }

        [JsonProperty("TotalLiquid")] public Ceiling TotalLiquid { get; set; }

        [JsonProperty("Rain")] public Ceiling Rain { get; set; }

        [JsonProperty("Snow")] public Ceiling Snow { get; set; }

        [JsonProperty("Ice")] public Ceiling Ice { get; set; }

        [JsonProperty("CloudCover")] public long CloudCover { get; set; }

        [JsonProperty("MobileLink")] public Uri MobileLink { get; set; }

        [JsonProperty("Link")] public Uri Link { get; set; }
    }

    public class Ceiling
    {
        [JsonProperty("Value")] public double Value { get; set; }

        //[JsonProperty("Unit")] public Unit Unit { get; set; }

        [JsonProperty("UnitType")] public long UnitType { get; set; }
    }

    public class Wind
    {
        [JsonProperty("Speed")] public Ceiling Speed { get; set; }

        [JsonProperty("Direction")] public Direction Direction { get; set; }
    }

    public class Direction
    {
        [JsonProperty("Degrees")] public long Degrees { get; set; }

        [JsonProperty("Localized")] public string Localized { get; set; }

        [JsonProperty("English")] public string English { get; set; }
    }

    public class WindGust
    {
        [JsonProperty("Speed")] public Ceiling Speed { get; set; }
    }

    public partial class AccuweatherHourlyForecast
    {
        public static AccuweatherHourlyForecast[] FromJson(string json)
        {
            return JsonConvert.DeserializeObject<AccuweatherHourlyForecast[]>(json, Converter.Settings);
        }
    }

    public static class Serialize
    {
        public static string ToJson(this AccuweatherHourlyForecast[] self)
        {
            return JsonConvert.SerializeObject((object)self, (JsonSerializerSettings)Converter.Settings);
        }
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {

                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            }
        };
    }
}
