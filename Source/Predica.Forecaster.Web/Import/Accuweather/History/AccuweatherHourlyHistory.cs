﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Predica.Forecaster.Web.Import.Accuweather.History
{

    public partial class AccuweatherHourlyHistory
    {
        [JsonProperty("LocalObservationDateTime")]
        public DateTimeOffset LocalObservationDateTime { get; set; }

        [JsonProperty("EpochTime")] public long EpochTime { get; set; }

        [JsonProperty("WeatherText")] public string WeatherText { get; set; }

        [JsonProperty("WeatherIcon")] public long WeatherIcon { get; set; }

        [JsonProperty("HasPrecipitation")] public bool HasPrecipitation { get; set; }

        [JsonProperty("PrecipitationType")] public string PrecipitationType { get; set; }

        [JsonProperty("IsDayTime")] public bool IsDayTime { get; set; }

        [JsonProperty("Temperature")] public ApparentTemperature Temperature { get; set; }

        [JsonProperty("RealFeelTemperature")] public ApparentTemperature RealFeelTemperature { get; set; }

        [JsonProperty("RealFeelTemperatureShade")]
        public ApparentTemperature RealFeelTemperatureShade { get; set; }

        [JsonProperty("RelativeHumidity")] public long RelativeHumidity { get; set; }

        [JsonProperty("DewPoint")] public ApparentTemperature DewPoint { get; set; }

        [JsonProperty("Wind")] public Wind Wind { get; set; }

        [JsonProperty("WindGust")] public WindGust WindGust { get; set; }

        [JsonProperty("UVIndex")] public long UvIndex { get; set; }

        //[JsonProperty("UVIndexText")] public UvIndexText UvIndexText { get; set; }

        [JsonProperty("Visibility")] public ApparentTemperature Visibility { get; set; }

        //[JsonProperty("ObstructionsToVisibility")]
        //public ObstructionsToVisibility ObstructionsToVisibility { get; set; }

        [JsonProperty("CloudCover")] public long CloudCover { get; set; }

        [JsonProperty("Ceiling")] public ApparentTemperature Ceiling { get; set; }

        [JsonProperty("Pressure")] public ApparentTemperature Pressure { get; set; }

        //[JsonProperty("PressureTendency")] public PressureTendency PressureTendency { get; set; }

        [JsonProperty("Past24HourTemperatureDeparture")]
        public ApparentTemperature Past24HourTemperatureDeparture { get; set; }

        [JsonProperty("ApparentTemperature")] public ApparentTemperature ApparentTemperature { get; set; }

        [JsonProperty("WindChillTemperature")] public ApparentTemperature WindChillTemperature { get; set; }

        [JsonProperty("WetBulbTemperature")] public ApparentTemperature WetBulbTemperature { get; set; }

        [JsonProperty("Precip1hr")] public ApparentTemperature Precip1Hr { get; set; }

        [JsonProperty("PrecipitationSummary")]
        public Dictionary<string, ApparentTemperature> PrecipitationSummary { get; set; }

        [JsonProperty("TemperatureSummary")] public TemperatureSummary TemperatureSummary { get; set; }

        [JsonProperty("MobileLink")] public Uri MobileLink { get; set; }

        [JsonProperty("Link")] public Uri Link { get; set; }
    }

    public class ApparentTemperature
    {
        [JsonProperty("Metric")] public Imperial Metric { get; set; }

        [JsonProperty("Imperial")] public Imperial Imperial { get; set; }
    }

    public class Imperial
    {
        [JsonProperty("Value")] public double Value { get; set; }

        [JsonProperty("Unit")] public string Unit { get; set; }

        [JsonProperty("UnitType")] public long UnitType { get; set; }
    }

    public class TemperatureSummary
    {
        [JsonProperty("Past6HourRange")] public PastHourRange Past6HourRange { get; set; }

        [JsonProperty("Past12HourRange")] public PastHourRange Past12HourRange { get; set; }

        [JsonProperty("Past24HourRange")] public PastHourRange Past24HourRange { get; set; }
    }

    public class PastHourRange
    {
        [JsonProperty("Minimum")] public ApparentTemperature Minimum { get; set; }

        [JsonProperty("Maximum")] public ApparentTemperature Maximum { get; set; }
    }

    public class Wind
    {
        [JsonProperty("Direction")] public Direction Direction { get; set; }

        [JsonProperty("Speed")] public ApparentTemperature Speed { get; set; }
    }

    public class Direction
    {
        [JsonProperty("Degrees")] public long Degrees { get; set; }

        [JsonProperty("Localized")] public string Localized { get; set; }

        [JsonProperty("English")] public string English { get; set; }
    }

    public class WindGust
    {
        [JsonProperty("Speed")] public ApparentTemperature Speed { get; set; }
    }

    public partial class AccuweatherHourlyHistory
    {
        public static AccuweatherHourlyHistory[] FromJson(string json)
        {
            return JsonConvert.DeserializeObject<AccuweatherHourlyHistory[]>(json, Converter.Settings);
        }
    }

    public static class Serialize
    {
        public static string ToJson(this AccuweatherHourlyHistory[] self)
        {
            return JsonConvert.SerializeObject(self, Converter.Settings);
        }
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter {DateTimeStyles = DateTimeStyles.AssumeUniversal}
            }
        };
    }
}
