﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Predica.Forecaster.Web.Import.Accuweather.Forecast;
using Predica.Forecaster.Web.Import.Accuweather.History;
using Predica.Forecaster.Web.Infrastructure.Data;

namespace Predica.Forecaster.Web.Import.Accuweather
{
    public class AccuweatherImporter : IMeasurementsRepository
    {
        private const string Address = "http://dataservice.accuweather.com";
        private const string ApplicationKey = "apikey=hShEAWsdD2ALwAyBlVUliTVByM4scJHn";
        private const string IncludeFullResponseDetails = "details=true";

        private ApplicationDbContext _context;

        public AccuweatherImporter(ApplicationDbContext context) => _context = context;

        public IEnumerable<ImportMeasurementsResponse> GetHistoricalConditions(List<int> demandLocations)
        {
            var history = new List<ImportMeasurementsResponse>();
            var locations = _context.LocationAccuweather.Where(x => demandLocations.Contains(x.LocationId)).ToList();
            using (var httpClient = new HttpClient())
            {
                var resourceUrl =
                    $"{Address}/currentconditions/v1/{{0}}/historical/24?{ApplicationKey}&{IncludeFullResponseDetails}";
                foreach (var location in locations)
                {
                    var requestUri = string.Format(resourceUrl, location.LocationAccuweatherId);
                    var jsonResponse = httpClient.GetStringAsync(requestUri).Result;
                    var perHours = MapHistory(jsonResponse, location.LocationId);
                    history.AddRange(perHours);
                }

                return history;
            }
        }

        public IEnumerable<ImportMeasurementsResponse> GetForecastConditions(List<int> demandLocations)
        {
            var forecast = new List<ImportMeasurementsResponse>();
            var locations = _context.LocationAccuweather.Where(x => demandLocations.Contains(x.LocationId)).ToList();
            using (var httpClient = new HttpClient())
            {
                // https://developer.accuweather.com/accuweather-forecast-api/apis/get/forecasts/v1/hourly/12hour/%7BlocationKey%7D
                var resourceUrl =
                    $"{Address}/forecasts/v1/hourly/12hour/{{0}}?{ApplicationKey}&{IncludeFullResponseDetails}&metric=true";
                foreach (var location in locations)
                {
                    var requestUri = string.Format(resourceUrl, location.LocationAccuweatherId);
                    var jsonResponse = httpClient.GetStringAsync(requestUri).Result;
                    var perHours = MapForecast(jsonResponse, location.LocationId);
                    forecast.AddRange(perHours);
                }

                return forecast;
            }
        }


        private static IEnumerable<ImportMeasurementsResponse> MapForecast(string jsonResponse, int locationId)
        {
            return AccuweatherHourlyForecast.FromJson(jsonResponse)
                .Select(f => new ImportMeasurementsResponse
                {
                    MeasuredDate = f.EpochDateTime,
                    Temperature = f.Temperature.Value,
                    FeelTemperature = f.RealFeelTemperature.Value,
                    WindSpeed = f.Wind.Speed.Value,
                    Humidity = f.RelativeHumidity,
                    LocationId = locationId
                });
        }

        private static IEnumerable<ImportMeasurementsResponse> MapHistory(string jsonResponse, int locationId)
        {
            return AccuweatherHourlyHistory.FromJson(jsonResponse)
                .Select(f => new ImportMeasurementsResponse
                {
                    MeasuredDate = f.EpochTime,
                    Temperature = f.Temperature.Metric.Value,
                    FeelTemperature = f.RealFeelTemperature.Metric.Value,
                    WindSpeed = f.Wind.Speed.Metric.Value,
                    Humidity = f.RelativeHumidity,
                    LocationId = locationId
                });
        }
    }
}