﻿using System.Collections.Generic;

namespace Predica.Forecaster.Web.Import
{
    public interface IMeasurementsRepository
    {
        IEnumerable<ImportMeasurementsResponse> GetHistoricalConditions(List<int> locations);
        IEnumerable<ImportMeasurementsResponse> GetForecastConditions(List<int> locations);
    }
}