﻿using System;

namespace Predica.Forecaster.Web.Infrastructure
{
    public class UtcDateTimeProvider : IDateTimeProvider
    {
        public DateTime Now() => DateTime.UtcNow;
        public long ToTimestamp(DateTime dateTime) => ((DateTimeOffset)dateTime).ToUnixTimeSeconds();
    }
}
