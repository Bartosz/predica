﻿using System;

namespace Predica.Forecaster.Web.Infrastructure
{
    public interface IDateTimeProvider
    {
        DateTime Now();
        long ToTimestamp(DateTime dateTime);
    }
}