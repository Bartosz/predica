﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Predica.Forecaster.Web.Infrastructure.Data.Migrations
{
    public partial class AccuweatherImportingMeasurmensts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    LocationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Latitude = table.Column<decimal>(nullable: false),
                    Longitude = table.Column<decimal>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    ModificationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.LocationId);
                });

            migrationBuilder.CreateTable(
                name: "HourlyMeasurement",
                columns: table => new
                {
                    LocationId = table.Column<int>(nullable: false),
                    MeasuredDate = table.Column<long>(nullable: false),
                    Temperature = table.Column<double>(nullable: false),
                    FeelsTemperature = table.Column<double>(nullable: false),
                    Humidity = table.Column<decimal>(nullable: false),
                    WindSpeed = table.Column<double>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "(GETDATE())"),
                    ModificationDate = table.Column<DateTime>(nullable: false, defaultValueSql: "(GETDATE())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HourlyMeasurement", x => new { x.LocationId, x.MeasuredDate });
                    table.ForeignKey(
                        name: "FK_HourlyMeasurement_Location_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LocationAccuweather",
                columns: table => new
                {
                    LocationAccuweatherId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LocationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationAccuweather", x => x.LocationAccuweatherId);
                    table.ForeignKey(
                        name: "FK_LocationAccuweather_Location_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Location",
                columns: new[] { "LocationId", "CreationDate", "Latitude", "Longitude", "ModificationDate", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 51.25m, 22.57m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lublin" },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 52.23547m, 21.04191m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Warszawa" },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 52.4082663m, 16.9335199m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Poznan" },
                    { 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 51.7687323m, 19.4569911m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Łódź" },
                    { 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50.0619474m, 19.9368564m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kraków" },
                    { 6, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 51.1089776m, 17.0326689m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Wrocław" },
                    { 7, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 54.347629m, 18.6452324m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gdansk" },
                    { 8, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 54.5164982m, 18.5402738m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gdynia" },
                    { 9, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 53.4301818m, 14.5509623m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Szczecin" },
                    { 10, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50.2598987m, 19.0215852m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Katowice" },
                    { 11, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50.0374531m, 22.0047174m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Rzeszów" },
                    { 12, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50.2780834m, 19.1342944m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sosnowiec" },
                    { 13, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 53.132398m, 23.1591679m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Białystok" },
                    { 14, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 50.8717388m, 20.6308626m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kielce" },
                    { 15, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 53.1219648m, 18.0002529m, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bydgoszcz" }
                });

            migrationBuilder.InsertData(
                table: "LocationAccuweather",
                columns: new[] { "LocationAccuweatherId", "LocationId" },
                values: new object[,]
                {
                    { 274231, 1 },
                    { 274663, 2 },
                    { 276594, 3 },
                    { 274340, 4 },
                    { 274455, 5 },
                    { 273125, 6 },
                    { 275174, 7 },
                    { 275175, 8 },
                    { 276655, 9 },
                    { 275781, 10 },
                    { 265516, 11 },
                    { 275788, 12 },
                    { 275110, 13 },
                    { 275941, 14 },
                    { 273875, 15 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_LocationAccuweather_LocationId",
                table: "LocationAccuweather",
                column: "LocationId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HourlyMeasurement");

            migrationBuilder.DropTable(
                name: "LocationAccuweather");

            migrationBuilder.DropTable(
                name: "Location");
        }
    }
}
