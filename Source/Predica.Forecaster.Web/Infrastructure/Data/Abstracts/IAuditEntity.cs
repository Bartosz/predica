﻿using System;

namespace Predica.Forecaster.Web.Infrastructure.Data.Abstracts
{
    public interface IAuditEntity
    {
        DateTime CreationDate { get; set; }
        DateTime ModificationDate { get; set; }
    }
}