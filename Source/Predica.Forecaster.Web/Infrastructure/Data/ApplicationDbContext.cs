﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Predica.Forecaster.Web.Infrastructure.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<HourlyMeasurement> HourlyMeasurement { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<LocationAccuweather> LocationAccuweather { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<LocationAccuweather>().HasKey(la => la.LocationAccuweatherId);
            modelBuilder.Entity<HourlyMeasurement>(hr =>
                {
                    hr.Property(p => p.CreationDate).HasDefaultValueSql("(GETDATE())");
                    hr.Property(p => p.ModificationDate).HasDefaultValueSql("(GETDATE())");
                    hr.HasKey(p => new {p.LocationId, p.MeasuredDate});
                }
            );


            var locations = new List<Location>
            {
                new Location {LocationId = 1, Latitude = 51.25m, Longitude = 22.57m, Name = "Lublin"},
                new Location {LocationId = 2, Latitude = 52.23547m, Longitude = 21.04191m, Name = "Warszawa"},
                new Location {LocationId = 3, Latitude = 52.4082663m, Longitude = 16.9335199m, Name = "Poznan"},
                new Location {LocationId = 4, Latitude = 51.7687323m, Longitude = 19.4569911m, Name = "Łódź"},
                new Location {LocationId = 5, Latitude = 50.0619474m, Longitude = 19.9368564m, Name = "Kraków"},
                new Location {LocationId = 6, Latitude = 51.1089776m, Longitude = 17.0326689m, Name = "Wrocław"},
                new Location {LocationId = 7, Latitude = 54.347629m, Longitude = 18.6452324m, Name = "Gdansk"},
                new Location {LocationId = 8, Latitude = 54.5164982m, Longitude = 18.5402738m, Name = "Gdynia"},
                new Location {LocationId = 9, Latitude = 53.4301818m, Longitude = 14.5509623m, Name = "Szczecin"},
                new Location {LocationId = 10, Latitude = 50.2598987m, Longitude = 19.0215852m, Name = "Katowice"},
                new Location {LocationId = 11, Latitude = 50.0374531m, Longitude = 22.0047174m, Name = "Rzeszów"},
                new Location {LocationId = 12, Latitude = 50.2780834m, Longitude = 19.1342944m, Name = "Sosnowiec"},
                new Location {LocationId = 13, Latitude = 53.132398m, Longitude = 23.1591679m, Name = "Białystok"},
                new Location {LocationId = 14, Latitude = 50.8717388m, Longitude = 20.6308626m, Name = "Kielce"},
                new Location {LocationId = 15, Latitude = 53.1219648m, Longitude = 18.0002529m, Name = "Bydgoszcz"}
            };

            var locationsAccuweather = new List<LocationAccuweather>
            {
                new LocationAccuweather {LocationId = 1, LocationAccuweatherId = 274231},
                new LocationAccuweather {LocationId = 2, LocationAccuweatherId = 274663},
                new LocationAccuweather {LocationId = 3, LocationAccuweatherId = 276594},
                new LocationAccuweather {LocationId = 4, LocationAccuweatherId = 274340},
                new LocationAccuweather {LocationId = 5, LocationAccuweatherId = 274455},
                new LocationAccuweather {LocationId = 6, LocationAccuweatherId = 273125},
                new LocationAccuweather {LocationId = 7, LocationAccuweatherId = 275174},
                new LocationAccuweather {LocationId = 8, LocationAccuweatherId = 275175},
                new LocationAccuweather {LocationId = 9, LocationAccuweatherId = 276655},
                new LocationAccuweather {LocationId = 10, LocationAccuweatherId = 275781},
                new LocationAccuweather {LocationId = 11, LocationAccuweatherId = 265516},
                new LocationAccuweather {LocationId = 12, LocationAccuweatherId = 275788},
                new LocationAccuweather {LocationId = 13, LocationAccuweatherId = 275110},
                new LocationAccuweather {LocationId = 14, LocationAccuweatherId = 275941},
                new LocationAccuweather {LocationId = 15, LocationAccuweatherId = 273875}
            };

            modelBuilder.Entity<Location>().HasData(locations);
            modelBuilder.Entity<LocationAccuweather>().HasData(locationsAccuweather);
        }
    }
}