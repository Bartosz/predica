﻿using System;
using Predica.Forecaster.Web.Infrastructure.Data.Abstracts;

namespace Predica.Forecaster.Web.Infrastructure.Data
{
    public class Location : IAuditEntity
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public LocationAccuweather LocationAccuweather { get; set; }

        #region IAuditEntity implementation

        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }

        #endregion
    }
}