﻿using System;
using Predica.Forecaster.Web.Infrastructure.Data.Abstracts;

namespace Predica.Forecaster.Web.Infrastructure.Data
{
    public class HourlyMeasurement : IAuditEntity
    {
        public int LocationId { get; set; }
        public Location Location { get; set; }
        public double Temperature { get; set; }
        public double FeelsTemperature { get; set; }
        public decimal Humidity { get; set; }
        public double WindSpeed { get; set; }
        public long MeasuredDate { get; set; }

        #region IAuditEntity implementation

        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }

        #endregion
    }
}